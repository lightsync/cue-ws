FROM golang:1.9-alpine AS build
RUN apk add -U --no-cache gcc musl-dev git
RUN mkdir -p /go/src/gitlab.com/lightsync/cue-ws
WORKDIR /go/src/gitlab.com/lightsync/cue-ws
COPY . .
RUN go build -a -o main ./

FROM gliderlabs/alpine
RUN apk add -U --no-cache ca-certificates
COPY --from=build /usr/local/go/lib/time/zoneinfo.zip /usr/local/go/lib/time/zoneinfo.zip
COPY --from=build /go/src/gitlab.com/lightsync/cue-ws/main /main
VOLUME [ "/data" ]
CMD ["/main"]
