package main

// MessageType represents the different messages types that can be sent /
// recieved.
type MessageType string

const (
	// StartTimestampMessage is for sync the event start time
	StartTimestampMessage MessageType = "START_TIMESTAMP"
)

// Message represents a websocket message
type Message struct {
	Type    MessageType    `json:"type"`
	Payload MessagePayload `json:"payload"`
}

// MessagePayload contains name and timestamp
type MessagePayload struct {
	Name      string `json:"name"`
	Timestamp int64  `json:"timestamp"`
}
