package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"strconv"
	"strings"
	"time"
)

const (
	// TimestampFilePath is where the timestamp will be saved
	TimestampFilePath = "./data/timestamp"
)

// Sync is used to keep a reference to the start time
type Sync struct {
	hub        *Hub
	name       string
	timestamp  int64
	currentMsg []byte
}

// NewSync initializes a new sync struct
func NewSync(hub *Hub) (*Sync, error) {
	s := &Sync{
		hub: hub,
	}

	b, err := ioutil.ReadFile(TimestampFilePath)
	if err != nil {
		if err := s.SetTimestamp(
			"default",
			time.Now().UnixNano()/int64(time.Millisecond),
		); err != nil {
			return nil, err
		}
	} else {
		parts := strings.Split(string(b), ",")
		name := strings.TrimSpace(parts[0])
		timestamp, err := strconv.ParseInt(strings.TrimSpace(parts[1]), 10, 64)
		if err != nil {
			return nil, err
		}
		s.name = name
		s.timestamp = timestamp
		_ = s.updateMessage()
	}

	return s, nil
}

func (s *Sync) newMessage() *Message {
	return &Message{
		Type: StartTimestampMessage,
		Payload: MessagePayload{
			Name:      s.name,
			Timestamp: s.timestamp,
		},
	}
}

func (s *Sync) newMessageBlob() ([]byte, error) {
	return json.Marshal(s.newMessage())
}

// HandleIncoming sends the start time to incoming clients
func (s *Sync) HandleIncoming(c *Client) {
	c.Send <- s.currentMsg
}

func (s *Sync) broadcast() {
	s.hub.Broadcast <- s.currentMsg
}

// SetTimestamp updates the timestamp
func (s *Sync) SetTimestamp(name string, timestamp int64) error {
	log.Printf("sync.SetTimestamp: %s,%d", name, timestamp)

	data := fmt.Sprintf("%s,%d", name, timestamp)
	if err := ioutil.WriteFile(
		TimestampFilePath,
		[]byte(data),
		0644,
	); err != nil {
		return err
	}

	s.name = name
	s.timestamp = timestamp
	_ = s.updateMessage()
	s.broadcast()

	return nil
}

func (s *Sync) updateMessage() error {
	msg, err := s.newMessageBlob()
	if err != nil {
		return err
	}

	s.currentMsg = msg
	return nil
}

// GetCurrentMessage gets the current timestamp message
func (s *Sync) GetCurrentMessage() []byte {
	return s.currentMsg
}
