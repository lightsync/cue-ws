package main

import (
	"fmt"
	"log"
	"net/http"
	"strconv"
	"strings"

	"github.com/gorilla/websocket"
	"github.com/spf13/viper"
)

var (
	upgrader = websocket.Upgrader{
		ReadBufferSize:  1024,
		WriteBufferSize: 1024,
		CheckOrigin: func(r *http.Request) bool {
			return true
		},
	}
)

func serveWs(hub *Hub, sync *Sync, w http.ResponseWriter, r *http.Request) {
	conn, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		if _, ok := err.(websocket.HandshakeError); !ok {
			log.Println(err)
		}
		return
	}

	client := NewClient(hub, conn)
	hub.Register <- client

	go client.Writer()
	go client.Reader()

	sync.HandleIncoming(client)
}

func handleTimeUpdate(sync *Sync, w http.ResponseWriter, r *http.Request) {
	name := r.URL.Query().Get("name")
	timestampStr := r.URL.Query().Get("startTimestamp")
	timestamp, err := strconv.ParseInt(
		strings.TrimSpace(timestampStr),
		10,
		64,
	)
	if name == "" {
		log.Panicf("handleTimeUpdate: name not set")
		return
	}
	if err != nil {
		log.Panicf("handleTimeUpdate: %v", err)
		return
	}

	if err := sync.SetTimestamp(name, timestamp); err != nil {
		log.Printf("handleTimeUpdate: %v", err)
	}

	w.WriteHeader(http.StatusNoContent)
}

func main() {
	viper.SetDefault("host", "0.0.0.0")
	viper.SetDefault("port", 8080)
	viper.SetDefault("admin_path", "asdf")
	viper.AutomaticEnv()

	hub := NewHub()
	go hub.Run()

	sync, err := NewSync(hub)
	if err != nil {
		log.Fatal(err)
	}

	mux := http.NewServeMux()

	mux.HandleFunc(
		fmt.Sprintf("/admin/%s", viper.GetString("admin_path")),
		func(w http.ResponseWriter, r *http.Request) {
			handleTimeUpdate(sync, w, r)
		})

	mux.HandleFunc("/ws", func(w http.ResponseWriter, r *http.Request) {
		serveWs(hub, sync, w, r)
	})

	if err := http.ListenAndServe(
		fmt.Sprintf("%s:%d", viper.GetString("host"), viper.GetInt("port")),
		mux,
	); err != nil {
		log.Fatal(err)
	}
}
