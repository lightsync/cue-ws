package main

import (
	"time"

	"github.com/gorilla/websocket"
)

const (
	// Time allowed to write the file to the client.
	writeWait = 10 * time.Second

	// Time allowed to read the next pong message from the client.
	pongWait = 60 * time.Second

	// Send pings to client with this period. Must be less than pongWait.
	pingPeriod = (pongWait * 9) / 10
)

// Client represents a websocket client connection
type Client struct {
	conn *websocket.Conn
	hub  *Hub
	Send chan []byte
}

// NewClient initializes a client struct from an incoming connection
func NewClient(hub *Hub, conn *websocket.Conn) *Client {
	return &Client{
		conn: conn,
		hub:  hub,
		Send: make(chan []byte),
	}
}

// Reader handles incoming websocket messages
func (c *Client) Reader() {
	defer func() {
		c.hub.Unregister <- c
		_ = c.conn.Close()
	}()

	c.conn.SetReadLimit(512)
	_ = c.conn.SetReadDeadline(time.Now().Add(pongWait))
	c.conn.SetPongHandler(func(string) error {
		_ = c.conn.SetReadDeadline(time.Now().Add(pongWait))
		return nil
	})

	for {
		// we dont care about any incoming messages
		_, _, err := c.conn.ReadMessage()
		if err != nil {
			break
		}
	}
}

// Writer handles outgoing messages
func (c *Client) Writer() {
	ticker := time.NewTicker(pingPeriod)

	defer func() {
		ticker.Stop()
		_ = c.conn.Close()
	}()

	for {
		select {
		case message, ok := <-c.Send:
			_ = c.conn.SetWriteDeadline(time.Now().Add(writeWait))

			if !ok {
				// The hub closed the channel.
				_ = c.conn.WriteMessage(websocket.CloseMessage, []byte{})
				return
			}

			if err := c.conn.WriteMessage(websocket.TextMessage, message); err != nil {
				return
			}

		case <-ticker.C:
			_ = c.conn.SetWriteDeadline(time.Now().Add(writeWait))
			if err := c.conn.WriteMessage(
				websocket.PingMessage,
				nil,
			); err != nil {
				return
			}
		}
	}
}
